FROM openjdk:11
ARG JAR_FILE=build/libs/config-server-0.0.1.jar
COPY ${JAR_FILE} config-server.jar
ENTRYPOINT ["java","-jar","/config-server.jar"]